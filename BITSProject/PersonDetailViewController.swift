//
//  PersonDetailViewController.swift
//  BITSProject
//
//  Created by Dzũng Lê on 17/11/16.
//  Copyright © 2016 Dzũng Lê. All rights reserved.
//

import UIKit
import MessageUI
import UserNotifications

class PersonDetailViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, MFMessageComposeViewControllerDelegate, MFMailComposeViewControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var personDetailImageView: UIImageView!
    @IBOutlet weak var personDetailName: UILabel!
    @IBOutlet weak var personDetailMoneyOwed: UITextField!
    @IBOutlet weak var personDetailTableView: UITableView!
    
    
    var person: Person?
    var textFieldArray = [UITextField]()
    let rowsInSection = [3, 2]
    
    func applyCircleImageMask(image: UIImageView!)
    {
        image.layer.masksToBounds = false
        image.layer.cornerRadius = image.frame.height/2
        image.clipsToBounds = true
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(isEditing)
        
        applyCircleImageMask(image: personDetailImageView)
        
        personDetailImageView.contentMode = UIViewContentMode.scaleAspectFill
        
        navigationItem.rightBarButtonItem = editButtonItem
        
        registerForKeyboardNotifications()
        
        textFieldArray.append(personDetailMoneyOwed)
        
        // Do any additional setup after loading the view.
        
        if let person = person
        {
            personDetailImageView.image = person.photo
            personDetailName.text = person.name
            personDetailMoneyOwed.text = person.formattedMoneyOwed
            personDetailMoneyOwed.adjustsFontSizeToFitWidth = true
        }
    }
    
    override func setEditing(_ editing: Bool, animated: Bool)
    {
        
        // User is not in editing mode. The edit button is still "Edit."
        
        if (!isEditing)
        {
            personDetailImageView.isUserInteractionEnabled = true
            for textField in textFieldArray
            {
                textField.isUserInteractionEnabled = true
                
                // textField.borderStyle = .bezel
                textField.layer.borderWidth = 0.5
            }
        }
            
            // User presses the "Done" button, which then cancels edit mode and turns the Done button back into "Edit".
        else
        {
            self.view.endEditing(true)
            
            personDetailImageView.isUserInteractionEnabled = false
            for textField in textFieldArray
            {
                textField.isUserInteractionEnabled = false
                textField.borderStyle = .roundedRect
                textField.layer.borderWidth = 0
            }
        }
        
        super.setEditing(editing, animated: true)
    }
    
    // Opens a menu where a reminder can be sent.
    @objc func openReminderOptions()
    {
        let ac = UIAlertController(title: "Send Reminder by...", message: nil, preferredStyle: .actionSheet)
        ac.addAction(UIAlertAction(title: "Text Message", style: .default, handler: sendReminderText))
        ac.addAction(UIAlertAction(title: "Email Message", style: .default, handler: sendReminderEmail))
        ac.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        present(ac, animated: true, completion: nil)
        
    }
    
    func sendReminderText(action: UIAlertAction!)
    {
        // If phone can send text.
        if MFMessageComposeViewController.canSendText() && person?.phoneNumber != nil
        {
            let controller = MFMessageComposeViewController()
            controller.body = "Hello, \(person!.name!)! You owe me \(person!.formattedMoneyOwed!)!"
            controller.recipients = ["\(person!.phoneNumber!)"]
            controller.messageComposeDelegate = self
            self.present(controller, animated: true, completion: nil)
            print("called")
            
        } else {
            showAlertMessage(title: "No phone number found", message: "There is no phone number for this person!")
        }
    }
    
    func sendReminderEmail(action: UIAlertAction!)
    {
        // If device can send email.
        if MFMailComposeViewController.canSendMail() && person?.phoneNumber != nil

        {
            let controller = MFMailComposeViewController()
            controller.mailComposeDelegate = self
            controller.setToRecipients([(person!.email!)])
            controller.setMessageBody("Hello, \(person!.name!)! You owe me \(person!.formattedMoneyOwed!)!", isHTML: true)
            
            present(controller, animated: true, completion: nil)
            
        }
            
        else
        {
             showAlertMessage(title: "No email address found", message: "There is no email address found for this person!")
            print("Mail send failed!")
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith: MessageComposeResult)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rowsInSection[section]
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0
        {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "infoCell") as! PersonDetailTableViewCell
            
            switch (indexPath.row) {
                
            case 0:
                
                // If person has phone number
                if let phoneNumber = person?.phoneNumber
                {
                    fillPersonTableViewCellString(cell: cell, cellTitle: "Phone Number", cellDetailInfo: phoneNumber, cellKeyboardType: .phonePad)
                }
                
            case 1:
                
                if let email = person?.email
                {
                    fillPersonTableViewCellString(cell: cell, cellTitle: "Email", cellDetailInfo: email, cellKeyboardType: .emailAddress)
                    
                }
                
            case 2:
                if let address = person?.address
                {
                    fillPersonTableViewCellString(cell: cell, cellTitle: "Address", cellDetailInfo: address, cellKeyboardType: .default)
                }
                
            default:
                break
                
            }
            
            return cell
        }
            
        else
            
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "buttonCell") as! PersonDetailButtonTableViewCell
            
            switch (indexPath.row)
            {
            case 0:
                cell.buttonCellButton.setTitle("Send Reminder", for: .normal)
                cell.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openReminderOptions)))
                
            case 1:
                cell.buttonCellButton.setTitle("Set Reminder", for: .normal)
                cell.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(scheduleReminder)))
                
            default: break
            }
            return cell
        }
        
    }
    
    @IBAction @objc func scheduleReminder()
    {
//        Reminder implementation
        if let selectedPerson = person
        {
            scheduleLocal(person: selectedPerson)
            print("Reminder scheduled")
            showAlertMessage(title: "Reminder Scheduled", message: "You will be reminded to ask for your money back in a week!")
        }

    }
    
    func fillPersonTableViewCellString(cell: PersonDetailTableViewCell, cellTitle: String, cellDetailInfo: String, cellKeyboardType: UIKeyboardType)
    {
        cell.personDetailCellLabel.text = cellTitle
        cell.personDetailCellLabelInfo.text = "\(cellDetailInfo)"
        
        textFieldArray.append(cell.personDetailCellLabelInfo)
        
        cell.personDetailCellLabel.sizeToFit()
        cell.personDetailCellLabelInfo.sizeToFit()
        
        cell.personDetailCellLabelInfo.keyboardType = cellKeyboardType
    }
    
    func registerLocal()
    {
        let center = UNUserNotificationCenter.current()
        
        center.requestAuthorization(options: [.alert, .badge, .sound])
        {
            (granted, error) in
            
            if granted
            {
                print("Nice")
            }
            else
            {
                print("No!")
            }
        }
    }
    
    func scheduleLocal(person: Person)
    {
        let center = UNUserNotificationCenter.current()
        
        let calendar = Calendar.current
        
        let dateFormatter = DateFormatter()
        
        switch NSLocale.current.identifier
        {
            case "en_US": dateFormatter.dateFormat = "MMM dd, yyyy"
            case "vi_VN": dateFormatter.dateFormat = "d MMM, yyyy"
        default: break;
        }

        let today = calendar.date(byAdding: .day, value: 7, to: Date.init())
        var dateComponents = DateComponents()
        
        guard let reminderDate = today else { return }
        
        dateComponents.day = calendar.component(.day, from: reminderDate)
        dateComponents.month = calendar.component(.month, from: reminderDate)
        dateComponents.year = calendar.component(.year, from: reminderDate)
        
        
        let content = UNMutableNotificationContent()
        content.title = "Reminder to ask for money!"
        content.body = "\(String(describing: person.name)) owes you \(String(describing: person.moneyOwed)) and you should ask for it!"
        content.sound = UNNotificationSound.default()
        
        let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: false)
        
        let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: trigger)
        
        center.add(request)
    }
    
    func showAlertMessage(title: String, message: String)
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alertController, animated: true, completion: nil)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    func registerForKeyboardNotifications() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: .UIKeyboardWillHide, object: nil)
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: .UIKeyboardWillChangeFrame, object: nil)
        
    }
    
    @objc func adjustForKeyboard(notification: Notification) {
        let userinfo = notification.userInfo!
        
        let keyboardScreenEndFrame = (userinfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
        
        if notification.name == Notification.Name.UIKeyboardWillHide {
            personDetailTableView.contentInset = UIEdgeInsets.zero
        } else {
            personDetailTableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height + 10, right: 0)
        }
        
        personDetailTableView.scrollIndicatorInsets = personDetailTableView.contentInset
        
    }
    
    @IBAction func selectImageFromPhotoLibrary(_ sender: UITapGestureRecognizer)
    {
        print("Photo selected")
        
        // Hides text field when selecting photo.
        self.view.endEditing(true)
        
        // Lets user pick a photo from their library.
        let imagePickerController = UIImagePickerController()
        
        // Only allow picking photos, not taken.
        imagePickerController.sourceType = .savedPhotosAlbum
        
        // Make ViewController notified when a user picks an image.
        imagePickerController.delegate = self
        
        // Presents the view controller.
        present(imagePickerController, animated: true, completion: nil)
        
    }
    
    // UIImagePickerControllerDelegate
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        let selectedImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        personDetailImageView.image = selectedImage
        
        personDetailImageView.contentMode = UIViewContentMode.scaleAspectFill
        personDetailImageView.clipsToBounds = true;
        person?.photo = selectedImage
        dismiss(animated: true, completion: nil)
        
    }
}

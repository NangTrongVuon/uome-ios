//
//  Person.swift
//  BITSProject
//
//  Created by Dzũng Lê on 31/10/16.
//  Copyright © 2016 Dzũng Lê. All rights reserved.
//

import UIKit

class Person: NSObject, NSCoding
{
    // Properties
    
    // Archiving Paths
    
    // Types
    
    struct PropertyKey {

        static let nameKey = "name"
        static let photoKey = "photo"
        static let moneyOwedKey = "moneyOwed"
        static let addressKey = "address"
        static let phoneNumberKey = "phoneNumber"
        static let emailKey = "email"
        static let dateKey = "date"
    }
    
    var name: String?
    var photo: UIImage?
    var moneyOwed: Double?
    var address: String?
    var phoneNumber: String?
    var email: String?
    var date: String?
    
    
    var formattedMoneyOwed: String?
    {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        
        var moneyOwedString: String?
        
        if let moneyOwedDouble = self.moneyOwed
        {
            moneyOwedString = formatter.string(from: NSNumber(value: moneyOwedDouble))
        }
        
        return moneyOwedString

    }
    
    static let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    static let archiveURL = documentsDirectory.appendingPathComponent("people")

    
    // Initializion
    
    init?(name: String, photo: UIImage?, moneyOwed: Double?, address: String?, phoneNumber: String?, email: String?, date: String?)
    {
        self.name = name
        self.photo = photo
        self.moneyOwed = moneyOwed
        self.address = address
        self.phoneNumber = phoneNumber
        self.email = email
        self.date = date
        
        super.init()
        
        if name.isEmpty
        {
            return nil
        }
    }
    
    func encode(with aCoder: NSCoder)
    {
        aCoder.encode(name, forKey: PropertyKey.nameKey)
        aCoder.encode(photo, forKey: PropertyKey.photoKey)
        aCoder.encode(moneyOwed, forKey: PropertyKey.moneyOwedKey)
        aCoder.encode(address, forKey: PropertyKey.addressKey)
        aCoder.encode(phoneNumber, forKey: PropertyKey.phoneNumberKey)
        aCoder.encode(email, forKey: PropertyKey.emailKey)
        aCoder.encode(date, forKey: PropertyKey.dateKey)
    }
    
    required convenience init?(coder aDecoder: NSCoder)
    {
        let name = aDecoder.decodeObject(forKey: PropertyKey.nameKey) as! String
        let photo = aDecoder.decodeObject(forKey: PropertyKey.photoKey) as? UIImage
        let moneyOwed = aDecoder.decodeObject(forKey: PropertyKey.moneyOwedKey) as! Double
        let address = aDecoder.decodeObject(forKey: PropertyKey.addressKey) as! String
        let phoneNumber = aDecoder.decodeObject(forKey: PropertyKey.phoneNumberKey) as! String
        let email = aDecoder.decodeObject(forKey: PropertyKey.emailKey) as! String
        let date = aDecoder.decodeObject(forKey: PropertyKey.dateKey) as! String
        
        self.init(name: name, photo: photo, moneyOwed: moneyOwed, address: address, phoneNumber: phoneNumber, email: email, date: date)
    }
    
//    func formattedMoneyOwed() -> String?
//    {
//        let formatter = NumberFormatter()
//        formatter.numberStyle = .currency
//        
//        let moneyOwedString = formatter.string(from: NSNumber(value: self.moneyOwed!))
//        
//        return moneyOwedString
//    }
    
   }

    

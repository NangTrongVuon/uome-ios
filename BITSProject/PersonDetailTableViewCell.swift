//
//  PersonDetailTableViewCell.swift
//  BITSProject
//
//  Created by Dzũng Lê on 17/11/16.
//  Copyright © 2016 Dzũng Lê. All rights reserved.
//

import UIKit

class PersonDetailTableViewCell: UITableViewCell {

    
    @IBOutlet weak var personDetailCellLabel: UILabel!
    @IBOutlet weak var personDetailCellLabelInfo: UITextField!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

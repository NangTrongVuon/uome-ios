//
//  ViewController.swift
//  BITSProject
//
//  Created by Dzũng Lê on 28/10/16.
//  Copyright © 2016 Dzũng Lê. All rights reserved.
//

import UIKit


class PersonViewController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    // Properties
    
    @IBOutlet weak var addPersonScrollView: UIScrollView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var moneyTextField: UITextField!
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var phoneNumberField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var datePicker: UIDatePicker!
    var activeField: UITextField?
    
    //    This is passed by PersonTableViewController or prepareForSegue.
    
    var person: Person?
    //    var datePicker: UIDatePicker?
    var isDatePickerHidden = true
    
    var owedDate: Date?
    
    var invalidTextFields: [UITextField] = []
    
    @IBAction func datePickerChanged(_ sender: Any) {
        owedDate = datePicker.date
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        owedDate = Date.init()
        
        // Do any additional setup after loading the view, typically from a nib.
        registerForKeyboardNotifications()
        
        // Handle text field user's input through delegate callbacks.
        nameTextField.delegate = self
        moneyTextField.delegate = self
        phoneNumberField.delegate = self
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))
        
        saveButton.isEnabled = false
        
        //        // Set up view if editing.
        //        if let person = person
        //        {
        //            navigationItem.title = person.name
        //            nameTextField.text = person.name
        //            moneyTextField.text = "\(person.moneyOwed ?? 0)"
        //            photoImageView.image = person.photo
        //        }
        
    }
    
    // Called when the user presses cancel.
    @IBAction func cancel(_ sender: UIBarButtonItem)
    {
        
        let isPresentingPersonInAddMode = presentingViewController is UINavigationController
        
        if isPresentingPersonInAddMode
        {
            dismiss(animated: true, completion: nil)
        }
        else
        {
            navigationController!.popViewController(animated: true)
        }
    }
    
    // Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        
        print("save pressed")
        if saveButton == (sender as? UIBarButtonItem)
        {
            print("save pressed")
            let name = nameTextField.text ?? "Unknown"
            let photo = photoImageView.image
            let moneyOwed = Double(moneyTextField.text ?? "0")
            let address = addressTextField.text ?? " "
            let phoneNumber = phoneNumberField.text ?? " "
            let email = emailField.text ?? " "
            
            let dateFormatter = DateFormatter()
            guard let unwrappedDate = owedDate else { return }
            
            dateFormatter.dateFormat = "yyyy-mm-dd"
            
            print(dateFormatter.string(from: unwrappedDate))
            dateFormatter.locale = Locale.current
            let date = dateFormatter.string(from: unwrappedDate)
            person = Person(name: name, photo: photo, moneyOwed: moneyOwed, address: address, phoneNumber: phoneNumber, email: email, date: date)
        }
    }
    
    
    // UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        checkValidInput(textField)
        
        if textField == nameTextField {
            navigationItem.title = textField.text
        }
        
        saveButton.isEnabled = invalidTextFields.isEmpty
    }
    
    func markValid(_ textField: UITextField, _ valid: Bool) {
        if valid {
            textField.layer.borderWidth = 0
            invalidTextFields = invalidTextFields.filter({$0 == textField})
        } else {
            textField.layer.borderColor = UIColor.red.cgColor
            textField.layer.borderWidth = 0.5
            textField.borderStyle = .roundedRect
            invalidTextFields.append(textField)
        }
    }
    
    func checkValidInput(_ textField: UITextField)
    {
        guard let textInField = textField.text else { return }
        
        switch textField {
        case nameTextField:
            if textInField.isEmpty {
                markValid(textField, false)
            } else {
                markValid(textField, true)
            }
            break
        case moneyTextField, phoneNumberField:
            if !isStringNumeric(textInField) {
                markValid(textField, false)
            } else {
                markValid(textField, true)
            }
            break
            
        default:
            break
        }
        //        saveButton.isEnabled = !name.isEmpty && !money.isEmpty && checkStringNumeric(money) && checkStringNumeric(phone)
        
        //        print(saveButton.isEnabled)
        activeField = nil
    }
    
    func isStringNumeric(_ string: String) -> Bool {
        let stringScanner = Scanner(string: string)
        return stringScanner.scanDecimal(nil) && stringScanner.isAtEnd
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        saveButton.isEnabled = false
        activeField = textField
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func selectImageFromPhotoLibrary(_ sender: UITapGestureRecognizer)
    {
        print("Photo selected")
        
        // Hides text field when selecting photo.
        self.view.endEditing(true)
        
        // Lets user pick a photo from their library.
        let imagePickerController = UIImagePickerController()
        
        // Only allow picking photos, not taken.
        imagePickerController.sourceType = .savedPhotosAlbum
        
        // Make ViewController notified when a user picks an image.
        imagePickerController.delegate = self
        
        // Presents the view controller.
        present(imagePickerController, animated: true, completion: nil)
        
    }
    
    // UIImagePickerControllerDelegate
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any])
    {
        let selectedImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        photoImageView.image = selectedImage
        photoImageView.contentMode = UIViewContentMode.scaleAspectFill
        photoImageView.clipsToBounds = true;
        
        dismiss(animated: true, completion: nil)
        
    }
    
    func registerForKeyboardNotifications() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: .UIKeyboardWillHide, object: nil)
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: .UIKeyboardWillChangeFrame, object: nil)
    }
    
    //    @objc func keyboardWasShown(_ notification: NSNotification) {
    //
    //        print("keyboard was shown")
    //
    //        guard let info = notification.userInfo,
    //        let keyboardFrameValue = info[UIKeyboardFrameBeginUserInfoKey] as? NSValue
    //            else { print("returned"); return }
    //
    //        let keyboardFrame = keyboardFrameValue.cgRectValue
    //        let keyboardSize = keyboardFrame.size
    //
    //        let contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.height, 0.0)
    //        addPersonScrollView.contentInset = contentInsets
    //        addPersonScrollView.scrollIndicatorInsets = contentInsets
    //    }
    //
    //    @objc func keyboardWillBeHidden(_ notification: NSNotification) {
    //
    //        print("keyboard was hidden")
    //        let contentInsets = UIEdgeInsets.zero
    //        addPersonScrollView.contentInset = contentInsets
    //        addPersonScrollView.scrollIndicatorInsets = contentInsets
    //    }
    
    @objc func adjustForKeyboard(notification: Notification) {
        let userinfo = notification.userInfo!
        
        let keyboardScreenEndFrame = (userinfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
        
        if notification.name == Notification.Name.UIKeyboardWillHide {
            addPersonScrollView.contentInset = UIEdgeInsets.zero
        } else {
            addPersonScrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height + 10, right: 0)
        }
        
        addPersonScrollView.scrollIndicatorInsets = addPersonScrollView.contentInset
    }

    //    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    //
    //        let validCharacters = CharacterSet.decimalDigits.description
    //        let dotCount = moneyTextField.text?.components(separatedBy: ".").count
    //
    //        guard let moneyCount = dotCount else { return false }
    //
    //        if moneyCount > 0 && string == "." || string != validCharacters {
    //            return false
    //        }
    //
    //        return true
    //    }
    
}



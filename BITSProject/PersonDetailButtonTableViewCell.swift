//
//  PersonDetailButtonTableViewCell.swift
//  BITSProject
//
//  Created by Dzũng Lê on 18/11/16.
//  Copyright © 2016 Dzũng Lê. All rights reserved.
//

import UIKit

class PersonDetailButtonTableViewCell: UITableViewCell {

    @IBOutlet weak var buttonCellButton: UIButton!
    @IBOutlet weak var buttonCellContentView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

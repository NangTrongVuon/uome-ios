//
//  PersonTableViewCell.swift
//  BITSProject
//
//  Created by Dzũng Lê on 31/10/16.
//  Copyright © 2016 Dzũng Lê. All rights reserved.
//

import UIKit

class PersonTableViewCell: UITableViewCell {

    // Properties
    
    @IBOutlet weak var personNameLabel: UILabel!
    @IBOutlet weak var personPhoto: UIImageView!
    @IBOutlet weak var personMoneyOwed: UILabel!
    @IBOutlet weak var personDate: UILabel!
    @IBAction func moreInfoButton(_ sender: UIButton) {
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        personPhoto.contentMode = UIViewContentMode.scaleAspectFill
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

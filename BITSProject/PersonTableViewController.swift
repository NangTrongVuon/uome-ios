//
//  PersonTableViewController.swift
//  BITSProject
//
//  Created by Dzũng Lê on 31/10/16.
//  Copyright © 2016 Dzũng Lê. All rights reserved.
//

import UIKit

class PersonTableViewController: UITableViewController {
    
    // Properties
    var people = [Person]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let savedPeople = loadPeople()
        {
            people += savedPeople
        }
        
        else
        {
            loadNewPeople()
        }
        
        tableView.reloadData()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Functions
    func loadNewPeople()
    {
        let dzung = Person(name: "Dzung", photo: #imageLiteral(resourceName: "defaultPhoto"), moneyOwed: 500, address: "31 Cach Mang Thang Tam", phoneNumber: "0906757985", email: "morepositive@outlook.com", date: "Aug 21, 2017")!
        
        people += [dzung]
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return people.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        // Table view cells are used and dequeued with a cell identifier.
        let cellIdentifier = "PersonTableViewCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! PersonTableViewCell
        
        // Fetch the proper cell's data.
        let person = people[indexPath.row]
        
        cell.personNameLabel.text = person.name
        cell.personPhoto.image = person.photo

        cell.personDate.text = person.date
        
        cell.personMoneyOwed.text = person.formattedMoneyOwed
        
        cell.personNameLabel.sizeToFit()
        cell.personMoneyOwed.sizeToFit()
        cell.personDate.sizeToFit()
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete
        {
            people.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
        
        if editingStyle == .insert
        {
            
        }
    }
    
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    @IBAction func unwindToPeopleList(sender: UIStoryboardSegue)
    {
        
        if let sourceViewController = sender.source as? PersonViewController, let person = sourceViewController.person
        {
            
            // Edit person.
            if let selectedIndexPath = tableView.indexPathForSelectedRow
            {
                people[selectedIndexPath.row] = person
                tableView.reloadRows(at: [selectedIndexPath], with: .none)
            }
                
            else
            {
                // Add new person.
                let newIndexPath = NSIndexPath(row: people.count, section: 0)
                people.append(person)
                tableView.insertRows(at: [newIndexPath as IndexPath], with: .bottom)
                
            }
            savePeople()
        }
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        
        if segue.identifier == "ShowDetail"
        {
            let peopleDetailViewController = segue.destination as! PersonDetailViewController
            
            // Get the cell that generated this segue (The cell the user touched)
            
            if let selectedPersonCell = sender as? PersonTableViewCell
            {
                let indexPath = tableView.indexPath(for: selectedPersonCell)!
                
                let selectedPerson = people[indexPath.row]
                peopleDetailViewController.person = selectedPerson
            }
            
        }
            
        else if segue.identifier == "AddItem"
        {
            
        }
        
        
    }
    
    // NSCoding
    
    func savePeople()
    {
        let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(people, toFile: Person.archiveURL.path)
        if !isSuccessfulSave
        {
            print("File save failed.")
        }
    }
    
    func loadPeople() -> [Person]?
    {
        return NSKeyedUnarchiver.unarchiveObject(withFile: Person.archiveURL.path) as? [Person]
    }
    
}
